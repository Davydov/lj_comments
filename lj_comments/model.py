from elixir import *


class User(Entity):
    id = Field(Integer, primary_key=True)
    name = Field(String(15), required=True)

    def __repr__(self):
        return '<User "%s" (%d)>' % (self.name, self.id)

class Comment(Entity):
    id = Field(Integer, primary_key=True)
    poster = ManyToOne('User')
    state = Field(String(1))
    jitemid = Field(Integer)
    parent = ManyToOne('Comment')
    childs = OneToMany('Comment')
    subject = Field(UnicodeText)
    body = Field(UnicodeText)
    date = Field(DateTime)
    # We ignore properties like IP.

    def __repr__(self):
        return '<Comment %d>' % self.id

def init(storage):
    metadata.bind = storage
    #metadata.bind.echo = True
    setup_all()
    create_all()
