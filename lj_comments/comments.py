import logging

import time
import datetime

from sqlalchemy import desc
from sqlalchemy.exceptions import IntegrityError, InvalidRequestError
from elixir import session

import lj

import model
from model import User, Comment


USER_AGENT = 'lj_comments (http://bitbucket.org/Davydov/lj_comments)'

# This is not real W3C time. We assume timezone always to be Z.
W3C_TIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

class Comments(object):
    def __init__(self, user, password, storage, journal=None, 
                 user_agent=USER_AGENT, server='www.livejournal.com'):
        self.server = lj.Server(user, password, user_agent, server)
        self.journal = journal
        model.init(storage)
        
    def update(self):
        logging.debug('updating meta')
        max_meta_id = self.update_meta()
        time.sleep(1)
        logging.debug('max_meta_id: %d' % max_meta_id)
        if max_meta_id:
            logging.debug('updating body')
            self.update_body(max_meta_id)

    def update_meta(self, max_id=None):
        if max_id is None:
            try:
                max_id = Comment.query.order_by(desc(Comment.id)).first().id
            except AttributeError:
                max_id = 0
        logging.debug('last known id %d' % max_id)
        result = self.server.get_comments_meta(max_id + 1,
                                                     self.journal)
        logging.debug('got %d users and %d comments' %
                (len(result['usermaps']), len(result['comments'])))
        if result['usermaps']:
            for user_id in result['usermaps']:
                try:
                    user = User(id=user_id)
                except IntegrityError:
                    user = User.query.filter(User.id == user_id).get()
                user.name = name=result['usermaps'][user_id]
            session.flush()

        if result['comments']:
            max_comment_id = 0 
            for comment_id in result['comments']:
                if comment_id > max_comment_id:
                    max_comment_id = comment_id
                    try:
                        comment = Comment(id=comment_id)
                    except IntegrityError:
                        comment = Comment.query.filter(
                            Comment.id == comment_id).get()
                    try:
                        comment.poster_id = result['comments'][comment_id]\
                            ['posterid']
                    except KeyError:
                        pass
                    try:
                        comment.state = result['comments'][comment_id]['state']
                    except KeyError:
                        comment.state = 'A'
            session.flush()
            if max_comment_id < result['maxid']:
                time.sleep(1)
                return self.update_meta(max_comment_id)
            return max_comment_id
        else:
            return max_id

    def update_body(self, max_meta_id=None, max_body_id=None):
        if max_body_id is None:
            try:
                max_body_id = Comment.query.filter(Comment.body != None).\
                    order_by(desc(Comment.id)).first().id
            except AttributeError:
                max_body_id = 0
        if max_meta_id is None:
            max_meta_id = Comment.query.order_by(desc(Comment.id)).first().id
        if max_meta_id == max_body_id:
            # We alreadey have every body.
            logging.debug('we already have all bodies (%d)' % max_body_id)
            return max_body_id
        result = self.server.get_comments_body(max_body_id + 1,
                                                     self.journal)
        if not result['comments']:
            if max_body_id + 1000 < max_meta_id:
                logging.debug('empty. calling again (%d)' % max_body_id+1000)
                return self.update_body(max_meta_id, max_body_id+1000)
            else:
                logging.debug('empty. finishing (%d)' % max_body_id)
                return max_body_id
        max_comment_id = 0
        for comment_id in result['comments']:
            logging.debug('comment %d: %s' % 
                          (comment_id, str(result['comments'][comment_id])))

            if comment_id > max_comment_id:
                max_comment_id = comment_id
            comment = Comment.query.filter(Comment.id == comment_id).one()
            comment.subject = result['comments'][comment_id]['subject']
            comment.body = result['comments'][comment_id]['body']
            if result['comments'][comment_id]['date']:
                comment.date = datetime.datetime.strptime(
                    result['comments'][comment_id]['date'],
                    W3C_TIME_FORMAT)
            comment.jitemid = result['comments'][comment_id]['jitemid']
            try:
                comment.parent_id = result['comments'][comment_id]['parentid']
            except KeyError:
                pass
        session.flush()
        if max_comment_id < max_meta_id:
            logging.debug('got not all. calling again (sleep 1)')
            time.sleep(1)
            return self.update_body(max_meta_id, max_comment_id)
        else:
            logging.debug('got everything. finishing (%d).' % max_comment_id)
            return max_comment_id

